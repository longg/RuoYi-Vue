package com.ruoyi.web.controller.manager;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.manager.domain.Protype;
import com.ruoyi.manager.service.IProtypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 产品规格Controller
 *
 * @author dali
 * @date 2023-01-07
 */
@RestController
@RequestMapping("/product/protype")
public class ProtypeController extends BaseController {
    @Autowired
    private IProtypeService protypeService;

    /**
     * 查询产品规格列表
     */
    @PreAuthorize("@ss.hasPermi('product:protype:list')")
    @GetMapping("/list")
    public TableDataInfo list(Protype protype){
        List<Protype> list = protypeService.selectProtypeList(protype);
        return getDataTable(list);
    }

    /**
     * 获取产品规格详细信息
     */
    @PreAuthorize("@ss.hasPermi('product:protype:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(protypeService.selectProtypeById(id));
    }

    /**
     * 新增产品规格
     */
    @PreAuthorize("@ss.hasPermi('product:protype:add')")
    @Log(title = "产品规格", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Protype protype)
    {
        return toAjax(protypeService.insertProtype(protype));
    }

    /**
     * 修改产品规格
     */
    @PreAuthorize("@ss.hasPermi('product:protype:edit')")
    @Log(title = "产品规格", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Protype protype)
    {
        return toAjax(protypeService.updateProtype(protype));
    }

    /**
     * 删除产品规格
     */
    @PreAuthorize("@ss.hasPermi('product:protype:remove')")
    @Log(title = "产品规格", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(protypeService.deleteProtypeByIds(ids));
    }
}
