-- ----------------------------
-- 1、产品规格
-- ----------------------------
drop table if exists m_protype;
create table m_protype (
    id                bigint(20)      not null auto_increment    comment 'id',
    type_name         varchar(50)     not null                   comment '类型名称',
    type_code         varchar(50)     not null                   comment '类型代码',
    type_value        varchar(1000)   not null                   comment '规格值：多个用英文逗号,分割',
    parent_id         bigint(20)      default 0                  comment '父菜单ID',
    order_num         int(4)          default 0                  comment '显示顺序',
    visible           char(1)         default 0                  comment '状态（0显示 1隐藏）',
    create_by         varchar(64)     default ''                 comment '创建者',
    create_time       datetime                                   comment '创建时间',
    update_by         varchar(64)     default ''                 comment '更新者',
    update_time       datetime                                   comment '更新时间',
    primary key (id)
) engine=innodb auto_increment=2000 comment = '产品规格';

-- ----------------------------
-- 2、产品信息
-- ----------------------------
drop table if exists m_product;
create table m_product (
    id                bigint(20)      not null auto_increment    comment 'id',
    product_name      varchar(200)    not null                   comment '产品名称',
    protype_id        varchar(1000)   not null                   comment '规格代码：多个规格用英文逗号,分割',
    remark            varchar(3000)   default ''                 comment '备注信息',
    visible           char(1)         default 0                  comment '状态（0显示 1隐藏）',
    create_by         varchar(64)     default ''                 comment '创建者',
    create_time       datetime                                   comment '创建时间',
    update_by         varchar(64)     default ''                 comment '更新者',
    update_time       datetime                                   comment '更新时间',
    primary key (id)
) engine=innodb auto_increment=2000 comment = '产品信息';

-- ----------------------------
-- 3、商品信息
-- ----------------------------
drop table if exists m_goods;
create table m_goods (
    id                bigint(20)      not null auto_increment    comment 'id',
    product_id        bigint(20)      not null                   comment '产品ID',
    remark            varchar(3000)   default ''                 comment '备注信息',
    visible           char(1)         default 0                  comment '状态（0显示 1隐藏）',
    create_by         varchar(64)     default ''                 comment '创建者',
    create_time       datetime                                   comment '创建时间',
    update_by         varchar(64)     default ''                 comment '更新者',
    update_time       datetime                                   comment '更新时间',
    primary key (id)
) engine=innodb auto_increment=2000 comment = '产品信息';

