-- 初始化类型
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(1,'幕墙配件','10','规格',null,1,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(2,'','20','300×300',1,1,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(8,'板面尺寸','10','板面尺寸',2,1,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(11,'','20','295×295',8,1,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(12,'','20','296×296',8,2,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(13,'','20','297×297',8,3,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(131,'','20','298×298',8,4,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(9,'中心距','10','中心距',2,2,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(14,'','20','90',9,1,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(15,'','20','100',9,2,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(16,'','20','132',9,3,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(17,'','20','135',9,4,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(18,'','20','140',9,5,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(19,'','20','145',9,6,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(20,'','20','150',9,7,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(10,'孔尺寸','10','孔尺寸',2,3,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(21,'','20','15×70',10,1,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(22,'','20','15×95',10,2,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(23,'','20','15×100',10,3,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(24,'','20','15×105',10,4,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(25,'','20','15×110',10,5,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(26,'','20','16×100',10,6,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(27,'','20','16×105',10,7,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(28,'','20','16×110',10,8,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(29,'','20','16×120',10,9,1,'ruoyi',sysdate());

INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(3,'','20','200×300',1,2,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(30,'板面尺寸','10','板面尺寸',3,1,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(31,'','20','179×279',30,1,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(32,'','20','184×284',30,2,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(33,'','20','185×285',30,3,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(34,'','20','186×286',30,4,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(35,'','20','192×292',30,5,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(36,'','20','193×293',30,6,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(37,'','20','194×294',30,7,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(38,'','20','195×295',30,8,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(39,'','20','196×296',30,9,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(40,'','20','197×297',30,10,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(41,'','20','198×298',30,11,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(42,'中心距','10','中心距',3,2,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(43,'','20','90',42,1,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(44,'','20','100',42,2,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(45,'','20','132',42,3,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(46,'','20','135',42,4,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(47,'','20','140',42,5,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(48,'','20','145',42,6,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(49,'','20','150',42,7,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(50,'孔尺寸','10','孔尺寸',3,3,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(51,'','20','15×70',50,1,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(52,'','20','15×85',50,2,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(53,'','20','15×95',50,3,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(54,'','20','15×100',50,4,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(55,'','20','15×105',50,5,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(56,'','20','15×110',50,6,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(57,'','20','16×100',50,7,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(58,'','20','16×105',50,8,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(59,'','20','16×110',50,9,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(60,'','20','16×115',50,10,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(61,'','20','16×120',50,11,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(62,'','20','18×115',50,12,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(63,'','20','18×120',50,13,1,'ruoyi',sysdate());






INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(4,'','20','200×250',1,3,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(5,'','20','200×200',1,4,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(6,'','20','200×150',1,5,1,'ruoyi',sysdate());
INSERT INTO m_protype (id,type_name,type_code,type_value,parent_id,order_num,visible,create_by,create_time)VALUES(7,'','20','150×150',1,6,1,'ruoyi',sysdate());