import request from '@/utils/request'

// 查询产品规格列表
export function listProtype(query) {
  return request({
    url: '/product/protype/list',
    method: 'get',
    params: query
  })
}

// 查询产品规格详细
export function getProtype(id) {
  return request({
    url: '/product/protype/' + id,
    method: 'get'
  })
}

// 新增产品规格
export function addProtype(data) {
  return request({
    url: '/product/protype',
    method: 'post',
    data: data
  })
}

// 修改产品规格
export function updateProtype(data) {
  return request({
    url: '/product/protype',
    method: 'put',
    data: data
  })
}

// 删除产品规格
export function delProtype(id) {
  return request({
    url: '/product/protype/' + id,
    method: 'delete'
  })
}
