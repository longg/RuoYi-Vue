package com.ruoyi.framework.netty;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * @author lizhije
 * @date 2023-09-17
 * 协议的处理程序，用于处理不同协议的请求
 */
public class ProtocolHandler extends SimpleChannelInboundHandler<Object> {
    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, Object o) throws Exception {
        // 在这里处理接收到的请求，例如解析协议、调用业务逻辑等
        System.out.println("Received request: " + o);
        // 响应客户端
        channelHandlerContext.writeAndFlush("Response to client");
    }
}
