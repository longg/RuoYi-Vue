package com.ruoyi.framework.netty;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * @author lizhije
 * @date 2023-09-17
 * 协议编码类，用于编码不同协议的请求数据。
 */
public class ProtocolEncoder extends MessageToByteEncoder<Object> {

    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, Object o, ByteBuf byteBuf) throws Exception {
        byte[] bytes = toBytes(o);
        byteBuf.writeBytes(bytes);
    }

    private byte[] toBytes(Object o) {
        return o.toString().getBytes();
    }
}
