package com.ruoyi.framework.netty;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

/**
 * @author lizhije
 * @date 2023-09-17
 * 协议解密类，用于解码不同协议的书
 */
public class ProtocolDecoder extends ByteToMessageDecoder {
    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out){
        in.markReaderIndex();
        byte data = in.readByte();
        if (data == 0) {
            byte[] bytes = new byte[in.readableBytes()];
            in.readBytes(bytes);
            out.add(new String(bytes));
        } else {
            out.add(data);
        }
        in.resetReaderIndex();
    }
}
