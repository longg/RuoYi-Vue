package com.ruoyi.manager.mapper;

import java.util.List;
import com.ruoyi.manager.domain.Protype;

/**
 * 产品规格Mapper接口
 *
 * @author dali
 * @date 2023-01-07
 */
public interface ProtypeMapper
{
    /**
     * 查询产品规格
     *
     * @param id 产品规格主键
     * @return 产品规格
     */
    public Protype selectProtypeById(Long id);

    /**
     * 查询产品规格列表
     *
     * @param protype 产品规格
     * @return 产品规格集合
     */
    public List<Protype> selectProtypeList(Protype protype);

    /**
     * 新增产品规格
     *
     * @param protype 产品规格
     * @return 结果
     */
    public int insertProtype(Protype protype);

    /**
     * 修改产品规格
     *
     * @param protype 产品规格
     * @return 结果
     */
    public int updateProtype(Protype protype);

    /**
     * 删除产品规格
     *
     * @param id 产品规格主键
     * @return 结果
     */
    public int deleteProtypeById(Long id);

    /**
     * 批量删除产品规格
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteProtypeByIds(Long[] ids);
}
