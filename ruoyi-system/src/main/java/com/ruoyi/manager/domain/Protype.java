package com.ruoyi.manager.domain;

import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 产品规格对象 m_protype
 *
 * @author dali
 * @date 2023-01-07
 */
public class Protype extends BaseEntity{
    private static final long serialVersionUID = 8983466262459200894L;

    /** id */
    private Long id;

    /** 类型名称 */
    private String typeName;

    /** 类型代码 */
    private String typeCode;

    /** 规格值：多个用英文逗号,分割 */
    private String typeValue;

    /** 父菜单ID */
    private Long parentId;

    /** 显示顺序 */
    private Integer orderNum;

    /** 状态（0显示 1隐藏） */
    private String visible;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setTypeName(String typeName)
    {
        this.typeName = typeName;
    }

    public String getTypeName()
    {
        return typeName;
    }
    public void setTypeCode(String typeCode)
    {
        this.typeCode = typeCode;
    }

    public String getTypeCode()
    {
        return typeCode;
    }
    public void setTypeValue(String typeValue)
    {
        this.typeValue = typeValue;
    }

    public String getTypeValue()
    {
        return typeValue;
    }
    public void setParentId(Long parentId)
    {
        this.parentId = parentId;
    }

    public Long getParentId()
    {
        return parentId;
    }
    public void setOrderNum(Integer orderNum)
    {
        this.orderNum = orderNum;
    }

    public Integer getOrderNum()
    {
        return orderNum;
    }
    public void setVisible(String visible)
    {
        this.visible = visible;
    }

    public String getVisible()
    {
        return visible;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("typeName", getTypeName())
                .append("typeCode", getTypeCode())
                .append("typeValue", getTypeValue())
                .append("parentId", getParentId())
                .append("orderNum", getOrderNum())
                .append("visible", getVisible())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .toString();
    }
}
