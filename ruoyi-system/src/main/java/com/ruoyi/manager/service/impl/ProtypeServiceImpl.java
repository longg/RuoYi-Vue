package com.ruoyi.manager.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.manager.domain.Protype;
import com.ruoyi.manager.mapper.ProtypeMapper;
import com.ruoyi.manager.service.IProtypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 产品规格Service业务层处理
 *
 * @author dali
 * @date 2023-01-07
 */
@Service
public class ProtypeServiceImpl implements IProtypeService
{
    @Autowired
    private ProtypeMapper protypeMapper;

    /**
     * 查询产品规格
     *
     * @param id 产品规格主键
     * @return 产品规格
     */
    @Override
    public Protype selectProtypeById(Long id)
    {
        return protypeMapper.selectProtypeById(id);
    }

    /**
     * 查询产品规格列表
     *
     * @param protype 产品规格
     * @return 产品规格
     */
    @Override
    public List<Protype> selectProtypeList(Protype protype)
    {
        return protypeMapper.selectProtypeList(protype);
    }

    /**
     * 新增产品规格
     *
     * @param protype 产品规格
     * @return 结果
     */
    @Override
    public int insertProtype(Protype protype)
    {
        protype.setCreateTime(DateUtils.getNowDate());
        return protypeMapper.insertProtype(protype);
    }

    /**
     * 修改产品规格
     *
     * @param protype 产品规格
     * @return 结果
     */
    @Override
    public int updateProtype(Protype protype)
    {
        protype.setUpdateTime(DateUtils.getNowDate());
        return protypeMapper.updateProtype(protype);
    }

    /**
     * 批量删除产品规格
     *
     * @param ids 需要删除的产品规格主键
     * @return 结果
     */
    @Override
    public int deleteProtypeByIds(Long[] ids)
    {
        return protypeMapper.deleteProtypeByIds(ids);
    }

    /**
     * 删除产品规格信息
     *
     * @param id 产品规格主键
     * @return 结果
     */
    @Override
    public int deleteProtypeById(Long id)
    {
        return protypeMapper.deleteProtypeById(id);
    }
}
